package jp.ac.uryukyu.ie.e185749;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
 
class YushaTest {
 
    @Test void attackWithWeaponSkillTest() {
            int enemyHp = 1000;
            int attack = 100;
            Warrior demoYusha = new Warrior("デモ勇者", 1, attack);
            Enemy slime = new Enemy("スライムもどき", enemyHp, 1);
            for(int count = 0; count < 3; count++){
                enemyHp = slime.hitPoint;
                demoYusha.attackWithWeponSkill(slime);
                assertEquals(enemyHp - attack*1.5, slime.hitPoint);
            }
    }
}