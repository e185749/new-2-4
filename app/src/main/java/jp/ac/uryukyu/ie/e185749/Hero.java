package jp.ac.uryukyu.ie.e185749;

public class Hero extends Living {
    public Hero(String name, int maximumHP, int attack){
        super(name, maximumHP, attack);
    }
   
    @Override
    public void wounded(int damage){
        hitPoint -= damage;
        if( hitPoint < 0 ) {
            dead = true;
            System.out.printf("勇者%sは力尽きてしまった。\n", getName());
        }
    }
}